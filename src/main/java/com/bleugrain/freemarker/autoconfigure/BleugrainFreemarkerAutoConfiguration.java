package com.bleugrain.freemarker.autoconfigure;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import freemarker.template.TemplateExceptionHandler;
import lombok.extern.slf4j.Slf4j;

@Configuration
@ConditionalOnClass(Configuration.class)
@EnableConfigurationProperties(FreemarkerProperties.class)
@Slf4j
public class BleugrainFreemarkerAutoConfiguration {
	
	@Autowired
	private FreemarkerProperties properties;
	
	@Autowired
	private ResourceLoader resourceLoader;
	
	@Bean
	@ConditionalOnMissingBean
	public freemarker.template.Configuration configuration() throws IOException {
		log.info("defining freemarker's configuration bean");
		freemarker.template.Configuration cfg = new freemarker.template.Configuration(freemarker.template.Configuration.VERSION_2_3_25);
		Resource templateDir = resourceLoader.getResource(properties.getTemplateDir());
		cfg.setDirectoryForTemplateLoading(templateDir.getFile());
		cfg.setDefaultEncoding(properties.getEncoding());
		cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
		cfg.setLogTemplateExceptions(properties.isLogExceptions());
		return cfg;
	}

	
}
