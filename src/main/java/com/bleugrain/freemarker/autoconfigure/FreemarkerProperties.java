package com.bleugrain.freemarker.autoconfigure;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ConfigurationProperties(prefix = "bleugrain.freemarker")
public class FreemarkerProperties {
	private String templateDir = "classpath:static/assets/emails/templates";
	private String encoding = "UTF-8";
	private boolean logExceptions = false;
	
}
